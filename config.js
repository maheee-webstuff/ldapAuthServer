
exports.PORT = 389;
exports.ADDRESS = '127.0.0.1';

exports.LOG_STREAMS = [
 {
   level: 'info',
   stream: process.stdout,
 },
 {
   level: 'debug',
   path: 'log.log',
 },
];

exports.USER = {
  'LDAPUser': {
    pass: 'VerySecretPassword',
    groups: []
  },
  'admin': {
    pass: 'admin',
    groups: ['admin-gui', 'manager-gui']
  },
  'user': {
    pass: 'pass',
    groups: ['AKS_MANAGER']
  },
};
