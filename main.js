var config = require('./config.js');
var ldap = require('ldapjs');
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: "l", streams: config.LOG_STREAMS});
var server = ldap.createServer({log: log});


function buildObject(id) {
  return {
    dn: 'cn='+id+',ou=Service Accounts,dc=company,dc=com',
    attributes: {
      cn: id,
    }
  };
}

function logSearch(req) {
  log.info("SEARCH: " + req.dn.toString())
  log.info("        filter: " + req.filter.toString());
}

server.bind('ou=Service Accounts,dc=company,dc=com', function(req, res, next) {
  log.info("BIND: " + req.dn.toString() + ":" + req.credentials)
  var user = config.USER[req.dn.rdns[0].cn];
  if (!user || user.pass !== req.credentials) {
    return next(new ldap.InvalidCredentialsError());
  }
  res.end();
  return next();
});

server.search('dc=User,dc=company,dc=com', function(req, res, next) {
  logSearch(req);
  
  var user = config.USER[req.filter.value];
  
  if (user) {
    res.send(buildObject(req.filter.value), true);
  }

  res.end();
});

server.search('dc=Groups,dc=company,dc=com', function(req, res, next) {
  logSearch(req);
  
  var userId = req.filter.value.match(/cn=([^,]+).*/)[1];
  var user = config.USER[userId];
  
  for (var i in user.groups) {
    res.send(buildObject(user.groups[i]), true);
  }
  
  res.end();
});

server.search('', function(req, res, next) {
  logSearch(req);
  res.end();
});

 server.listen(config.PORT, config.ADDRESS, function() {
   log.info('LDAP server listening at: ' + server.url);
 });
 
 