ldapAuthServer
==============

Tomcat Setup
------------

```
	<Realm
	   className="org.apache.catalina.realm.JNDIRealm" 
	   connectionURL="ldap://localhost:389"
	   authentication="simple"
	   referrals="follow"
	   connectionName="cn=LDAPUser,ou=Service Accounts,dc=company,dc=com"
	   connectionPassword="VerySecretPassword" 
	   userSearch="(sAMAccountName={0})"
	   userBase="dc=User,dc=company,dc=com" 
	   userSubtree="true"
	   roleSearch="(member={0})" 
	   roleName="cn" 
	   roleSubtree="true"
	   roleBase="dc=Groups,dc=company,dc=com" />
```
